#pragma once

#include <memory>
#include <vector>

#include <nodes/NodeDataModel>

#include <MatExpression.h>

#include "MatGraphNodeData.h"

namespace MatGraph {

class MatGraphNode : public QtNodes::NodeDataModel {

public:

    virtual
    ~MatGraphNode() {}

    virtual unsigned int
    nPorts(QtNodes::PortType portType) const override;

    virtual QtNodes::NodeDataType
    dataType(QtNodes::PortType portType, QtNodes::PortIndex portIndex) const override;

    virtual std::shared_ptr<QtNodes::NodeData>
    outData(QtNodes::PortIndex port) override;

    virtual void
    setInData(std::shared_ptr<QtNodes::NodeData> nodeData, QtNodes::PortIndex port) override;

    virtual QWidget *
    embeddedWidget() override { return nullptr; };

    std::shared_ptr<Mat::MatExpression> getExpression() { return expression; }

protected:

    std::shared_ptr<Mat::MatExpression> expression;

    std::vector<std::shared_ptr<MatGraphNodeData>> outputs;

};

}

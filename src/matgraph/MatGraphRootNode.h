#pragma once

#include <memory>

#include <nodes/NodeDataModel>

#include <Material.h>

namespace MatGraph {

class MatGraphRootNode : public QtNodes::NodeDataModel {

public:

    MatGraphRootNode(std::shared_ptr<Mat::Material> mat);

    virtual
    ~MatGraphRootNode() {}

    virtual QString
    caption() const override { return "Material"; }

    virtual QString
    name() const override { return "Material"; }

    virtual unsigned int
    nPorts(QtNodes::PortType portType) const override;

    virtual QtNodes::NodeDataType
    dataType(QtNodes::PortType portType, QtNodes::PortIndex portIndex) const override;

    virtual std::shared_ptr<QtNodes::NodeData>
    outData(QtNodes::PortIndex port) override;

    virtual void
    setInData(std::shared_ptr<QtNodes::NodeData> nodeData, QtNodes::PortIndex port) override;

    virtual QWidget *
    embeddedWidget() override { return nullptr; };

protected:

    std::shared_ptr<Mat::Material> material;

};

}

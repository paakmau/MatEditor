#pragma once

namespace Mat {

class HLSLTranslator {

public:

    HLSLTranslator();

    int Add(int a, int b);

    int Sub(int a, int b);

    int Mul(int a, int b);

    int Div(int a, int b);

};

}

#pragma once

#include <memory>
#include <vector>

#include <QString>

namespace Mat {

class MatExpression;

struct MatExpressionOutputMasks {

    MatExpressionOutputMasks(QString name, int mask = 0, int maskR = 0, int maskG = 0, int maskB = 0, int maskA = 0) {
        this->name = name;
        this->mask = mask;
        this->maskR = maskR;
        this->maskG = maskG;
        this->maskB = maskB;
        this->maskA = maskA;
    }

    QString name;

    // Whether to use mask
    int mask;

    int maskR;

    int maskG;

    int maskB;

    int maskA;

};

struct MatExpressionInput {

    void connect(std::shared_ptr<MatExpression> expr, int outputIdx, MatExpressionOutputMasks * masks) {
        input = expr;
        outputIndex = outputIdx;
        mask = masks->mask;
        maskR = masks->maskR;
        maskG = masks->maskG;
        maskB = masks->maskB;
        maskA = masks->maskA;
    }

    void disconnect() {
        input = std::shared_ptr<MatExpression>();
    }

    std::shared_ptr<MatExpression> input;

    int outputIndex;

    int mask;

    int maskR;

    int maskG;

    int maskB;

    int maskA;

};

class MatExpression {

public:

    MatExpression();

    virtual int
    getNumInputs() = 0;

    virtual int
    getNumOutputs() = 0;

    QString
    getInputName(int const & idx) { return inputNames[idx]; }

    QString
    getOutputName(int const & idx) { return outputMasks[idx].name; }

    virtual std::vector<MatExpressionInput * >
    getInputs() = 0;

    MatExpressionOutputMasks *
    getOutputMasks (int idx) { return &outputMasks[idx]; }

    void
    connect(std::shared_ptr<MatExpression> expr, int inputIndex, int outputIndex) {
        auto inputs = getInputs();
        inputs[inputIndex]->connect(expr, outputIndex, expr->getOutputMasks(outputIndex));
    }

    void
    disconnect(int inputIndex) {
        auto inputs = getInputs();
        inputs[inputIndex]->disconnect();
    }

protected:

    // 需要在派生类构造函数中初始化 TODO: 用反射
    std::vector<QString> inputNames;

    // 需要在派生类构造函数中初始化
    std::vector<MatExpressionOutputMasks> outputMasks;

};

}

#pragma once

#include <vector>

#include "MatExpression.h"

namespace Mat {

class MatExpressionAdd : public MatExpression
{

public:

    MatExpressionAdd();

    virtual int
    getNumInputs() override { return 2; }

    virtual int
    getNumOutputs() override { return 1; }

    virtual std::vector<MatExpressionInput * >
    getInputs() override {
        std::vector<MatExpressionInput * > res;
        res.push_back(&A);
        res.push_back(&B);
        return res;
    }

protected:

    MatExpressionInput A;

    MatExpressionInput B;

};

}

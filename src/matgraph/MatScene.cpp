#include "MatScene.h"

#include <nodes/Node>

#include "MatGraphNode.h"
#include "MatGraphRootNode.h"

using namespace std;
using namespace QtNodes;
using namespace Mat;

namespace MatGraph {

MatScene::MatScene(shared_ptr<Material> mat, shared_ptr<DataModelRegistry> registry, QObject * parent, const bool & autoCreateRoot) : FlowScene(registry, parent), material(mat){
    QObject::connect(this, &MatScene::nodeCreated, [this] (Node &node) {
        material->addExpression(((MatGraphNode*)node.nodeDataModel())->getExpression());
    });

    if (autoCreateRoot)
        createNode(make_unique<MatGraphRootNode>(material));
}

}

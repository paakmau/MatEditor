#include "MatGraphNodeAdd.h"

#include <MatExpressionAdd.h>

using namespace std;
using namespace QtNodes;
using namespace Mat;

namespace MatGraph {

MatGraphNodeAdd::MatGraphNodeAdd() {
    expression = make_shared<MatExpressionAdd>();

    for (int i = 0; i < expression->getNumOutputs(); i++)
        outputs.emplace_back(make_shared<MatGraphNodeData> (expression, i));
}

}

#include "MatGraphNode.h"

#include <MatExpression.h>

using namespace std;
using namespace QtNodes;
using namespace Mat;

namespace MatGraph {

unsigned int
MatGraphNode::nPorts(PortType portType) const {
    if (portType == PortType::In)
        return expression->getNumInputs();
    else
        return expression->getNumOutputs();
}

NodeDataType
MatGraphNode::dataType(PortType portType, PortIndex idx) const {
    // TODO: 获取NodeDataType的id
    if (portType == PortType::In) {
        QString type = expression->getInputName(idx);
        return NodeDataType { "Test", type };
    }
    else {
        QString type = expression->getOutputName(idx);
        return NodeDataType { "Test", type };
    }
}

shared_ptr<NodeData>
MatGraphNode::outData(PortIndex idx) {
    return outputs[idx];
}

void
MatGraphNode::setInData(shared_ptr<NodeData> data, PortIndex idx) {
    shared_ptr<MatGraphNodeData> matGraphNodeData = dynamic_pointer_cast<MatGraphNodeData>(data);

    if (matGraphNodeData)
        expression->connect(matGraphNodeData->getExpression(), idx, matGraphNodeData->getIndex());
    else
        expression->disconnect(idx);
}

}

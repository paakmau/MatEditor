#include "MatGraphRootNode.h"

#include "MatGraphNodeData.h"

using namespace std;
using namespace QtNodes;
using namespace Mat;

namespace MatGraph {

MatGraphRootNode::MatGraphRootNode(std::shared_ptr<Material> mat) {
    material = mat;
}

unsigned int
MatGraphRootNode::nPorts(PortType portType) const {
    if (portType == PortType::In)
        return material->getNumInputs();
    else
        return 0;
}

NodeDataType
MatGraphRootNode::dataType(PortType portType, PortIndex idx) const {
    // TODO: 获取NodeDataType的id
    if (portType == PortType::In) {
        QString type = material->getInputName(idx);
        return NodeDataType { "Test", type };
    }
    return NodeDataType();
}

shared_ptr<NodeData>
MatGraphRootNode::outData(PortIndex) {
    return shared_ptr<NodeData>();
}

void
MatGraphRootNode::setInData(shared_ptr<NodeData> data, PortIndex idx) {
    shared_ptr<MatGraphNodeData> matGraphNodeData = dynamic_pointer_cast<MatGraphNodeData>(data);
    if (matGraphNodeData)
        material->connect(matGraphNodeData->getExpression(), idx, matGraphNodeData->getIndex());
    else
        material->disconnect(idx);
}


}

#pragma once

#include <memory>
#include <vector>

#include "MatExpression.h"

namespace Mat {

class Material
{

public:

    Material();

    void
    addExpression(std::shared_ptr<MatExpression> && expr) {
        expressions.emplace_back(expr);
    }

    int
    getNumInputs() { return 5; }

    QString
    getInputName(int idx) {
        std::vector<QString> arr = { "BaseColor", "Metallic", "Specular", "Roughness", "Normal" };
        return arr[idx];
    }

    void
    connect(std::shared_ptr<MatExpression> expr, int inputIndex, int outputIndex) {
        getExpr(inputIndex)->connect(expr, outputIndex, expr->getOutputMasks(outputIndex));
    }

    void
    disconnect(int inputIndex) {
        getExpr(inputIndex)->disconnect();
    }

protected:

    MatExpressionInput * getExpr(int idx) {
        std::vector<MatExpressionInput * > arr = { &baseColor, &metallic, &specular, &roughness, &normal };
        return arr[idx];
    }

    std::vector<std::shared_ptr<MatExpression>> expressions;

    MatExpressionInput baseColor;

    MatExpressionInput metallic;

    MatExpressionInput specular;

    MatExpressionInput roughness;

    MatExpressionInput normal;

};

}

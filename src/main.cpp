#include <QApplication>
#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QMenuBar>

#include <nodes/NodeData>
#include <nodes/FlowScene>
#include <nodes/FlowView>
#include <nodes/DataModelRegistry>
#include <nodes/Node>

#include <MatScene.h>
#include <MatGraphNodeAdd.h>

#include <Material.h>

using namespace std;
using namespace QtNodes;
using namespace Mat;
using namespace MatGraph;

static shared_ptr<DataModelRegistry>
genDataModelRegistry() {
    auto res = make_shared<DataModelRegistry>();
    res->registerModel<MatGraphNodeAdd>("Addition");
    return res;
}

int
main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QWidget mainWidget;
    mainWidget.setWindowTitle("MatEditor");
    mainWidget.resize(800, 600);

    auto layout = new QVBoxLayout(&mainWidget);
    layout->setMargin(0);
    layout->setSpacing(0);

    auto menuBar = new QMenuBar(&mainWidget);
    auto clearAction = menuBar->addAction("Clear");
    auto showCodeAction = menuBar->addAction("Show code");
    layout->addWidget(menuBar);

    auto mat = make_shared<Material> ();

    auto scene = new MatScene(mat, genDataModelRegistry(), &mainWidget);
    auto view = new FlowView(scene);
    layout->addWidget(view);

    QObject::connect(clearAction, &QAction::triggered, [scene]() {
        scene->clear();
    });
    QObject::connect(showCodeAction, &QAction::triggered, []() {

    });

    mainWidget.show();

    return a.exec();
}

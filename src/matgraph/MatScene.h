#pragma once

#include <memory>

#include <nodes/FlowScene>

#include <Material.h>

namespace MatGraph {

// TODO: 应该勇敢的魔改nodes，没有虚方法很难操作
class MatScene : public QtNodes::FlowScene {

public:

    MatScene(std::shared_ptr<Mat::Material> mat, std::shared_ptr<QtNodes::DataModelRegistry> registry, QObject * parent = nullptr, const bool & autoCreateRoot = true);

protected:

    std::shared_ptr<Mat::Material> material;

};

}

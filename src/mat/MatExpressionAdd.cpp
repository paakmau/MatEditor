#include "MatExpressionAdd.h"

namespace Mat {

MatExpressionAdd::MatExpressionAdd() {
    inputNames.clear();
    inputNames.emplace_back("A");
    inputNames.emplace_back("B");
    outputMasks.clear();
    outputMasks.emplace_back(MatExpressionOutputMasks(""));
}

}

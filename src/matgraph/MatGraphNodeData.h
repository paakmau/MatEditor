#pragma once

#include <memory>

#include <QString>

#include <nodes/NodeDataModel>

#include <MatExpression.h>

namespace MatGraph {

class MatGraphNodeData : public QtNodes::NodeData {

public:

    MatGraphNodeData(std::shared_ptr<Mat::MatExpression> expr, QtNodes::PortIndex idx) : expression(expr), index(idx) {}

    virtual QtNodes::NodeDataType
    type() const override {
        return QtNodes::NodeDataType { "MatGraphNodeData", "MatGraphNodeData" };
    }

    std::shared_ptr<Mat::MatExpression>
    getExpression() const {
        return expression;
    }

    unsigned int getIndex() const {
        return index;
    }

private:

    std::shared_ptr<Mat::MatExpression> expression;

    QtNodes::PortIndex index;

};

}

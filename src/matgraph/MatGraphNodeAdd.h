#pragma once

#include "MatGraphNode.h"

namespace MatGraph {

class MatGraphNodeAdd : public MatGraphNode {

public:

    MatGraphNodeAdd();

    virtual QString
    caption() const override { return "Addition"; }

    virtual QString
    name() const override { return "Addition"; }

};

}
